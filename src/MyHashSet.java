import java.util.*;

public class MyHashSet<E> extends AbstractSet<E> implements Set<E> {

    private transient HashMap<E, Object> map;

    // Мнимое значение, необходимое для связи с Map
    private static final Object PRESENT = new Object();

    // Конструкторы
    public MyHashSet() {
        map = new HashMap<>();
    }

    public MyHashSet(int initialCapacity) {
        map = new HashMap<>(initialCapacity);
    }

    public MyHashSet(int initialCapacity, float loadFactor) {
        map = new HashMap<>(initialCapacity, loadFactor);
    }

    // Функции
    public boolean add(E e) {
        return map.put(e, PRESENT) == null;
    }

    public boolean contains(Object o) {
        return map.containsKey(o);
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public Iterator<E> iterator() {
        return map.keySet().iterator();
    }

    public boolean remove(Object o) {
        return map.remove(o) == PRESENT;
    }

    public int size() {
        return map.size();
    }

    public void clear() {
        map.clear();
    }

}

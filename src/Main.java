import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        /* Комментарий:
        * На самом деле, реализация HashMap - это, как я понял, для каких-то смельчаков,
        * т.к я попытался реализовать, посидел с прикреплённой статьёй и оригинальной реализацией часа 3 и понял,
        * что моя голова на пределе, а там ещё реализация красно-чёрного дерева, которая что-то уж очень страшная. Ну хоть идею с хеш-таблицей понял.

        Тесты:
        MyHashSet<Integer> set = new MyHashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        for (Integer element : set) {
            System.out.println(element);
        }
        System.out.println("-----------");

        Iterator<Integer> iterator = set.iterator();
        System.out.println(iterator.next());
        System.out.println("-----------");

        set.remove(2);
        for (Integer element : set) {
            System.out.println(element);
        }
        System.out.println("-----------");

        System.out.println(set.contains(1));
        System.out.println("Size: " +set.size());
        set.clear();
        System.out.println("-----------");
        System.out.println(set.contains(1));
        System.out.println("Size: " +set.size());
         */
    }
}